Recetarios IAPOS
================

Este sistema permite imprimir recetarios de medicamentos de IAPOS con todos los
datos completos, o sea, con los campos rellenos, listos para firmar, sellar y
entregar en la farmacia.

Para hacerlo, se necesita conseguir la plantilla de un recetario en blanco, el
cual se obtiene de la página de forma casi automática, pero para conseguirlo es
necesario marcar una cookie como `SameSite=None` paso que debe ser realizado de
forma manual por el usuario utilizando las herramientas de desarrollador del
navegador (utilizando F12 y haciendo un par de clicks).

Los datos se almacenan en el navegador y persisten entre sesiones, también es
posible exportarlos e importarlos en otro navegador. La mayoría de la magia
es gracias a la librería [Mavo][] que da vida al proyecto y a un SVG diseñado
utilizando [Method Draw][].

[Mavo]: https://mavo.io/
[Method Draw]: http://github.com/duopixel/Method-Draw/

## Licencia

Este programa es software libre bajo los términos de la licencia
AGPL-3.0-or-later, puede consultar los términos en el archivo
[COPYING](COPYING.md).
