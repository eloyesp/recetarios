const increment_version = () => {
  if (Mavo.all.recetarios === undefined) {
    return
  }
  const old_version = Mavo.all.recetarios.root.children.version.value
  Mavo.all.recetarios.root.children.version.value = old_version + 1
}
